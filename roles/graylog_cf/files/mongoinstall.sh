#!/bin/bash
file="/var/local/ismaster"
if [ -f $file ]
then
   hosts=$(cut -f 3 /var/local/instancegroup)
   firsthost=$(echo $hosts | cut -d " " -f 1 )
   otherhosts=$(echo $hosts | cut -d " " -f 2-)
   mongo --eval "$(echo "rs.initiate({ _id: \"graylog\", members: [ { _id: 0,  host: \"$firsthost\",  priority: 5 } ]} )")"
   echo otherhosts=$otherhosts
   for host in $otherhosts:
   do
         echo $(echo "rs.add(\"$host\")" | sed -e 's/://g')
         mongo --eval $(echo "rs.add(\"$host\")" | sed -e 's/://g')
         sleep 5s
   done
   #below is logic to remove instances that have been terminated.
   removedhosts=$(mongo --eval 'rs.status().members' | grep \"name\" | sed -e 's/["|,]//g' -e 's/ //g' | cut -d ":" -f 2 > /var/local/mongoinstances && cut -f 3 /var/local/instancegroup > /var/local/awsinstances && diff /var/local/mongoinstances  /var/local/awsinstances | grep \< | cut -d " " -f 2)
   for host in  $removedhosts ;
   do
   echo "rs.remove(\"$host:27017\")"
   mongo --eval $(echo "rs.remove(\"$host:27017\")")
   done
fi


